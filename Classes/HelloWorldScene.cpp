#include "HelloWorldScene.h"
#include "CustomTableViewCell.h"
//#include "ExtensionsTest.h"


USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

CCMenu* closeMenu;

#define WINSIZE CCDirector::sharedDirector()->getWinSize()


CCArray* _titlesArray, *_descriptionArray, *_linkArray;
CCTableView* tableView;
CCLabelTTF* _statusLabel;

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !CCLayerColor::initWithColor(ccc4(120, 120, 120, 255)))
    {
        return false;
    }
    
    _statusLabel = CCLabelTTF::create("loading ...", "Helvetica", WINSIZE.width/24 /*30.0*/);
    this->addChild(_statusLabel);
    _statusLabel->setPosition(ccp(WINSIZE.width/2, WINSIZE.height/2));
    
    CCMenuItemSprite* item = CCMenuItemSprite::create(CCSprite::create("CloseNormal.png"), CCSprite::create("CloseSelected.png"), this, menu_selector(HelloWorld::closeWebView));
    item->setPosition(ccp(WINSIZE.width - 40,  WINSIZE.height - (WINSIZE.height - WINSIZE.width)/2));
//    this->addChild(item, 100);
    
    // create menu, it's an autorelease object
    closeMenu = CCMenu::create(item, NULL);
    closeMenu->setPosition( CCPointZero );
    this->addChild(closeMenu, 100);
    closeMenu->setVisible(false);

    
    CCHttpRequest* request = new CCHttpRequest();
    // required fields
//    request->setUrl("http://httpbin.org/ip");
    request->setUrl("http://habrahabr.ru/rss/feed/posts/c28a5007c46606a270f8b6a535dd6710/");
    
    request->setRequestType(CCHttpRequest::kHttpGet);
    request->setResponseCallback(this, httpresponse_selector(HelloWorld::onHttpRequestCompleted));
    // optional fields
    request->setTag("GET test2");
    CCHttpClient::getInstance()->send(request);
    // don't forget to release it, pair to new
    request->release();
    
    return true;
}

void HelloWorld::closeWebView(){
    #if (!CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    if (m_webView)
        m_webView->removeWebView();
    
    closeMenu->setVisible(false);
#endif
}

void HelloWorld::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
    CCLOG("cell touched at index: %i", cell->getIdx());
#if (!CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    if (m_webView)
        m_webView->removeWebView();

    m_webView = new ZYWebView();
    m_webView->init();
    
    //Maybe you should handle coordinate convert between cocos2d-x coordinate and UIView coordinate, to make the webview's rect scaleable with the screen size.
    CCLOG("w: %f, h: %f", WINSIZE.width, WINSIZE.height);
    m_webView->showWebView(((CCString*)_linkArray->objectAtIndex(cell->getIdx()))->getCString(), 20, (WINSIZE.height - WINSIZE.width)/2 + 20, WINSIZE.width - 40, WINSIZE.width - 40);
    
    closeMenu->setVisible(true);
#endif
}

CCSize HelloWorld::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
//    if (idx == 2) {
//        return CCSizeMake(100, 100);
//    }
//    return CCSizeMake(60, 60);
    
//    return CCSizeMake(220, 65);
    return CCSizeMake(220, WINSIZE.height/10);
}

CCTableViewCell* HelloWorld::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
    CCString *string = CCString::createWithFormat("%d", idx);
    CCTableViewCell *cell = table->dequeueCell();
    if (!cell) {
        cell = new CustomTableViewCell();
        cell->autorelease();
        CCSprite *sprite = CCSprite::create("habr.png");
        sprite->setAnchorPoint(CCPointZero);
        sprite->setPosition(ccp(10, 0));
        cell->addChild(sprite);
        
//        CCLabelTTF *label = CCLabelTTF::create(string->getCString(), "Helvetica", 20.0);
//        CCLabelTTF *label = CCLabelTTF::create(((CCString*)_titlesArray->objectAtIndex(idx))->getCString(), CGSizeMake(200, 0), kCCTextAlignmentCenter,  "Helvetica", 15.0);
        
        CCLabelTTF *label = CCLabelTTF::create("", "Helvetica", 28.0, CCSizeMake(WINSIZE.width - 100, 60), kCCTextAlignmentCenter);
        
//        CCLOG("x %s", label->getString());
//        CCLOG("((CCString*)_titlesArray->objectAtIndex(idx))->getCString(): %s", ((CCString*)_titlesArray->objectAtIndex(idx))->getCString());
        
//        label->setPosition(CCPointZero);
        label->setPosition(ccp(90 , 60));
        label->setAnchorPoint(ccp(0, 1));
        label->setTag(123);
        cell->addChild(label);
    }
    else
    {
        CCLabelTTF *label = (CCLabelTTF*)cell->getChildByTag(123);
//        label->setString(string->getCString());
        label->setString(((CCString*)_titlesArray->objectAtIndex(idx))->getCString());
        
    }
    
    
    return cell;
}

unsigned int HelloWorld::numberOfCellsInTableView(CCTableView *table)
{
//    return 3;
     return _titlesArray->count();
}


void HelloWorld::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    _statusLabel->setString("onHttpRequestCompleted");
    
    if (!response)
    {
        return;
    }
    
    _statusLabel->setString("!response");
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLog("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
//    m_labelStatusCode->setString(statusString);
    CCLog("response code: %d", statusCode);
    CCLog("test");
    
//    _statusLabel->setString("statusCode");
    
     CCLog("test2");
    
    if (!response->isSucceed())
    {
        CCLog("test3");
        
        CCLog("response failed");
        CCLog("error buffer: %s", response->getErrorBuffer());
        _statusLabel->setString(response->getErrorBuffer());
        return;
    }
    
    CCLog("test4");
    
//    _statusLabel->setString("!response failed");
    
    CCLog("test5");
    
        const char* pXmlBuffer;
    CCString* xmlData = CCString::create("");
    
    CCLog("test6");
    
    // dump data
    std::vector<char> *buffer = response->getResponseData();
    
    CCLog("test7");
    
     CCLog("test8: %c%c%c%c%c", (*buffer)[0], (*buffer)[1],(*buffer)[2],(*buffer)[3],(*buffer)[4]);
    
    const char * bufferChar;
    std::string str = "";
    for (unsigned int i = 0; i < buffer->size(); i++)
    {
        printf("%c", (*buffer)[i]);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
        xmlData = CCString::createWithFormat("%s%c", xmlData->getCString(), (*buffer)[i]);
#endif
        str+=(*buffer)[i];
        
    }
//    printf("\n");
    CCLog("bufferChar: %s", bufferChar);
    
    #if (!CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    xmlData = CCString::create(str.c_str());
#endif
    
    CCLog("xmlDoc");
    
    tinyxml2::XMLDocument* xmlDoc = new tinyxml2::XMLDocument();

    CCLog("xmlDoc parse");
    xmlDoc->Parse(xmlData->getCString());
    tinyxml2::XMLElement* element = xmlDoc->FirstChildElement( "rss" )->FirstChildElement("channel")->FirstChildElement("item");
    CCLog("xmlDoc assert");
    CCAssert(element != NULL, "Cannot find ROOTELEMENT root Node");
    
    _titlesArray = CCArray::create();
    _descriptionArray = CCArray::create();
    _linkArray = CCArray::create();
    
    CCLog("_titlesArray _descriptionArray");
    while(element)
    {
        _titlesArray->addObject(CCString::create(element->FirstChildElement("title")->GetText()));
        _descriptionArray->addObject(CCString::create(element->FirstChildElement("description")->GetText()));
        _linkArray->addObject(CCString::create(element->FirstChildElement("guid")->GetText()));
        
        element = element->NextSiblingElement("item");
    }
    _titlesArray->retain();
    _descriptionArray->retain();
    _linkArray->retain();
    
//    for (int i = 0; i < _titlesArray->count(); i++) {
//        CCLOG("title: %s", ((CCString*)_titlesArray->objectAtIndex(i))->getCString());
//        CCLOG("description: %s", ((CCString*)_descriptionArray->objectAtIndex(i))->getCString());
//    }
    
    _statusLabel->setString("description");
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    CCLog("tableView");
    
    tableView = CCTableView::create(this, CCSizeMake(winSize.width, winSize.height));
    
    tableView->setDirection(kCCScrollViewDirectionVertical);
    //        tableView->setPosition(ccp(winSize.width-150,winSize.height/2-120));
    tableView->setPosition(ccp(0,0));
    tableView->setDelegate(this);
    tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
    this->addChild(tableView, 1);
    //    tableView->reloadData();
    tableView->reloadData();

    _statusLabel->setString("");
}
